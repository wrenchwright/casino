<?php
namespace Wainwright\CasinoDog\Controllers;
use Illuminate\Contracts\Support\Arrayable;
use JsonSerializable;
use Wainwright\CasinoDog\Controllers\Game\GameKernelTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use DB;
use Wainwright\CasinoDog\CasinoDog;

class TestingController
{
    use GameKernelTrait;
    public function __construct() {

        if(env('APP_ENV') !== 'local') {
            abort(400, 'Only available in APP_ENV=local');
        }
    }
    public function handle($function = NULL, Request $request) {
        if($function !== 'pulse_super_diamond_wild_game.html') {
        return $this->$function($request);
        }
    }
    protected function replaceInFile($search, $replace, $path)
    {
        file_put_contents($path, str_replace($search, $replace, file_get_contents($path)));
    }

    public function test_get_config(Request $request)
    {
        $game_controller = config('casino-dog.games');

        if (file_exists(config_path('../.wainwright/casino-dog/config/casino-dog.php'))) {
            $this->replaceInFile($game_controller, $game_controller.$game_controller, config_path('casino-dog.php'));
        }

        return config_path('casino-dog.games');
    }


}
